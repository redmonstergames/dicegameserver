// maintains set of redis connection pools
package redisPool

import (
	"github.com/garyburd/redigo/redis"
	"strconv"
	"time"
)

var pools = make(map[string]*redis.Pool)

func newPool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			curTime := strconv.FormatInt(time.Now().Unix(), 10)
			_, err := c.Do("SET", "RedisPool_laststarted", curTime)
			return err
		},
	}
}

// get a redis connection pool for the specific server
func GetPool(server string) *redis.Pool {
	if pool, ok := pools[server]; ok {
		return pool
	}
	pool := newPool(server)
	pools[server] = pool
	return pool
}
