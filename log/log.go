package log

import (
	"fmt"
	l "log"
	"net/http"
	"os"
)

const (
	ERROR = iota
	INFO
	DEBUG
)

const (
	ERROR_PREFIX   = "E :"
	INFO_PREFIX    = "I :"
	REQUEST_PREFIX = "R :"
	DEBUG_PREFIX   = "D :"
)

const (
	COLOR_RED     = "\x1b[31m"
	COLOR_GREEN   = "\x1b[32m"
	COLOR_YELLOW  = "\x1b[33m"
	COLOR_DEFAULT = "\x1b[0m"
)

var (
	setLevel = DEBUG
)

func doLog(level int, prefix string, color string, v ...interface{}) {
	// no null check required, this will fail on load if there is any error
	if level <= setLevel {
		if level == ERROR {
			l.SetOutput(os.Stderr)
		} else {
			l.SetOutput(os.Stdout)
		}
		l.Println(color, prefix, v, COLOR_DEFAULT)
	}
}

func OverrideLogLevel(level int) {
	setLevel = DEBUG
	if level == ERROR {
		setLevel = ERROR
	} else if level == INFO {
		setLevel = INFO
	}
}

func Info(v ...interface{}) {
	doLog(INFO, INFO_PREFIX, COLOR_GREEN, v)
}

func Debug(v ...interface{}) {
	doLog(DEBUG, DEBUG_PREFIX, COLOR_DEFAULT, v)
}

func Error(v ...interface{}) {
	doLog(ERROR, ERROR_PREFIX, COLOR_RED, v)
}

func LogRequest(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		msg := fmt.Sprintf("%s %s %s %v", r.RemoteAddr, r.Method, r.URL, r)
		l.Println(COLOR_YELLOW, REQUEST_PREFIX, msg, COLOR_DEFAULT)

		handler.ServeHTTP(w, r)
	})
}
