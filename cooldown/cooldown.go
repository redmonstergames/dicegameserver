package cooldown

import (
	"strconv"
	"time"

	"github.com/kiraninbng/go-redis/redis"
)

type CoolDown struct {
	redisServer  *redis.Client
	cooldownTime int64
	keyPrefix    string
}

func NewCoolDown(redisServer *redis.Client, cooldownTime int64, keyPrefix string) *CoolDown {
	cd := new(CoolDown)
	cd.redisServer = redisServer
	cd.cooldownTime = cooldownTime
	cd.keyPrefix = keyPrefix

	return cd
}

func (cd *CoolDown) Set(key string, vals []string) {
	newKey := cd.getKey(key)
	cd.purgeVals(newKey)

	now := time.Now().Unix() + cd.cooldownTime
	nowStr := strconv.FormatInt(now, 10)

	for _, val := range vals {
		cd.redisServer.HSet(newKey, val, nowStr)
	}

	// set the expiry for the main hash key
	cd.redisServer.Expire(newKey, int(cd.cooldownTime))

}

func (cd *CoolDown) Get(key string) (map[string]string, error) {
	newKey := cd.getKey(key)

	cd.purgeVals(newKey)
	return cd.redisServer.HGetAll(newKey)
}

func (cd *CoolDown) getKey(key string) string {
	return cd.keyPrefix + key
}

func (cd *CoolDown) purgeVals(key string) {
	now := time.Now().Unix()

	if cds, err := cd.redisServer.HGetAll(key); err == nil {
		for token, setTime := range cds {
			curTime, _ := strconv.ParseInt(setTime, 10, 64)
			if curTime < now {
				cd.redisServer.HDel(key, token)
			}
		}
	}
}
