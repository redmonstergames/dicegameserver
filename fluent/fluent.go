package fluent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/redmonstergames/stickcricketserver/utils"
)

const (
	TAG_USERS      = "mysql.users"
	TAG_COUNT      = "mysql.count"
	TAG_DAU        = "mysql.dau"
	TAG_INSTALL    = "mysql.install"
	TAG_ASSOCIATE  = "mysql.associate"
	TAG_MESSAGE    = "mysql.message"
	TAG_ECONOMY    = "mysql.economy"
	TAG_PAYMENT    = "mysql.payment"
	TAG_AFEVENTS   = "mysql.afevents"
	TAG_MATCH      = "mysql.match"
	TAG_MATCHOVERS = "mysql.matchovers"
	TAG_MATCHBALL  = "mysql.matchballs"
)

var statUrl string

func SetupFluent(server string) {
	statUrl = "http://" + server + ":9880/"
}

func LogUsers(data string) {

	//var data = map[string]interface{}{
	//	"user_id":  "4569dddd854354",
	//	"sn_id": "1",
	//	"sn_uid": "12323",
	//	"iap_purchase":"333333",
	//	"city":"Bangalore",
	//	"state":"Karnataka",
	//	"country":"India",
	//	"device":"unknown",
	//	"client_id":"1",
	//}

	//jsonStr, _ := json.Marshal(data)
	go Log("mysql.users", data)
}

func LogCount(data string) {
	updatedData := updateWithDate(data)

	go Log(TAG_COUNT, updatedData)
}

func LogMatch(data string) {
	updatedData := updateWithDate(data)

	go Log(TAG_MATCH, updatedData)
}

func LogDau(data string) {

	updatedData := updateWithDate(data)

	go Log(TAG_DAU, updatedData)
}

func LogInstall(data string) {
	updatedData := updateWithDate(data)

	go Log(TAG_INSTALL, updatedData)
}

func LogAFEvent(data string) {
	go Log(TAG_AFEVENTS, data)
}

func LogMatchOver(data string) {
	go Log(TAG_MATCHOVERS, data)
}

func LogMatchBall(data string) {
	go Log(TAG_MATCHBALL, data)
}

func LogAssociate(data string) {
	updatedData := updateWithDate(data)

	go Log(TAG_ASSOCIATE, updatedData)
}

func LogPayment(data string) {
	updatedData := updateWithDate(data)

	go Log(TAG_PAYMENT, updatedData)
}

func LogMessage(data string) {
	// the client will valid date - dont worry about client datetime issues

	var f interface{}
	json.Unmarshal([]byte(data), &f)

	if f == nil {
		return
	}

	statsData := f.(map[string]interface{})

	item, ok := statsData["date"]

	if !ok || item == nil || item == "" {
		now := utils.GetISTTimeNow()
		y, m, d := now.Date()
		statsData["date"] = fmt.Sprintf("%v-%v-%v %v:%v:00", y, int(m), d, now.Hour(), now.Minute())

		updatedData, _ := json.Marshal(statsData)
		go Log(TAG_MESSAGE, string(updatedData))
		return
	}

	go Log(TAG_MESSAGE, data)
}

func LogEconomy(data string) {

	updatedData := updateWithDate(data)
	go Log(TAG_ECONOMY, updatedData)
}

func LogStats(tag, data string) {
	switch tag {
	case TAG_USERS:
		LogUsers(data)
	case TAG_COUNT:
		LogCount(data)
	case TAG_DAU:
		LogDau(data)
	case TAG_MESSAGE:
		LogMessage(data)
	case TAG_ECONOMY:
		LogEconomy(data)
	case TAG_ASSOCIATE:
		LogAssociate(data)
	case TAG_INSTALL:
		LogInstall(data)
	case TAG_PAYMENT:
		LogPayment(data)
	case TAG_MATCH:
		LogMatch(data)
	case TAG_MATCHOVERS:
		LogMatchOver(data)
	case TAG_MATCHBALL:
		LogMatchBall(data)

	}
}

func Log(tag, data string) {
	client := &http.Client{}
	req, _ := http.NewRequest("POST", statUrl+tag, bytes.NewBuffer([]byte(data)))
	req.Header.Set("Content-Type", "application/json")
	res, _ := client.Do(req)
	if res != nil {
		res.Body.Close()
	} else {
		log.Println("No response from fluent...")
	}
}

func updateWithDate(data string) string {

	var f interface{}
	json.Unmarshal([]byte(data), &f)

	statsData := f.(map[string]interface{})
	now := utils.GetISTTimeNow()
	y, m, d := now.Date()
	statsData["date"] = fmt.Sprintf("%v-%v-%v %v:%v:00", y, int(m), d, now.Hour(), now.Minute())

	updatedData, _ := json.Marshal(statsData)
	return string(updatedData)
}
