package utils

import (
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"net/smtp"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	//	"reflect"
	//"fmt"

	redigo "github.com/garyburd/redigo/redis"
	"github.com/kiraninbng/go-redis/redis"
)

var ErrInvalidType = errors.New("Invalid type for conversion")

type StringSlice []string

const LOCATION = "Asia/Kolkata"

func LogOnError(err error) {
	if err != nil {
		log.Println("LogOnError error:", err)
	}
}

func QuitOnError(err error) {
	if err != nil {
		log.Println("QuitOnError error:", err.Error())
		os.Exit(1)
	}
}

func RandInt(min int, max int) int {
	if min < 0 {
		min = 0
	}
	if max < 0 {
		max = 0
	}
	if min >= max {
		return max
	}
	return min + rand.Intn(max-min)
}

func Iface2Float(a interface{}) (float64, error) {
	//	log.Info("type of interdace - ", reflect.TypeOf(a))

	switch a.(type) {
	case float64:
		return a.(float64), nil
	}
	return 0, ErrInvalidType
}

func Iface2UInt(a interface{}) (uint64, error) {
	//	log.Info("type of interdace - ", reflect.TypeOf(a))

	switch a.(type) {
	case uint64:
		return a.(uint64), nil
	case uint32:
		return uint64(a.(uint32)), nil	
	case float64:
		return uint64(a.(float64)), nil
	}
	return 0, ErrInvalidType
}

func Iface2Int(a interface{}) (int64, error) {
	//	log.Info("type of interdace - ", reflect.TypeOf(a))

	switch a.(type) {
	case int64:
		return a.(int64), nil
	case int:
		return int64(a.(int)), nil
	case float64:
		return int64(a.(float64)), nil
	}
	return 0, ErrInvalidType
}

func Iface2String(a interface{}) (string, error) {
	//	log.Info("type of interdace - ", reflect.TypeOf(a))

	switch a.(type) {
	case string:
		return a.(string), nil
	}
	return "", ErrInvalidType
}

func Iface2Bool(a interface{}) (bool, error) {
	//log.Info("type of interdace - ", reflect.TypeOf(a))

	switch a.(type) {
	case bool:
		return a.(bool), nil
	}
	return false, ErrInvalidType
}

func GetRootPath() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		LogOnError(err)
	}
	return dir
}

func Lock(rc *redis.Client, key string) string {
	for {
		curTime := strconv.FormatInt(time.Now().Unix(), 10)
		err := rc.SetWithExNx(key+"_lock", curTime, 1)
		if err == nil {
			return curTime
		}
		//log.Info("trying aquiring lock....", key, err)
		time.Sleep(500 * time.Millisecond)
	}
	return ""
}

func Unlock(rc *redis.Client, key string, randLock string) {
	rc.Eval("if redis.call('get',KEYS[1]) == ARGV[1] then return redis.call('del',KEYS[1]) else return 0 end", 1,
		[]string{key + "_lock"}, []string{randLock})
}

func GetISTTimeNow() time.Time {
	t := time.Now()
	return ConvertToISTTime(t)
}

func ConvertToISTTime(t time.Time) time.Time {
	l, err := time.LoadLocation(LOCATION)
	if err == nil && l != nil {
		t = t.In(l)
	} else { // do it manually
		t = t.UTC()
		istDiff := 5*time.Hour + 30*time.Minute
		t = t.Add(istDiff)
	}

	return t
}

func BeginningOfTheDay(t time.Time) time.Time {
	now := t
	midnight := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())

	return midnight
}

func IsBeforeToday(chkTime uint64) bool {
	curIST := ConvertToISTTime(time.Unix(int64(chkTime), 0))
	midnightIST := BeginningOfTheDay(GetISTTimeNow())

	if curIST.Unix() <= midnightIST.Unix() {
		return true
	}

	return false

}

func IsCurrentTimeBetween(start, end int) bool {
	now := GetISTTimeNow()
	startTime := time.Date(now.Year(), now.Month(), now.Day(), start, 0, 0, 0, now.Location())
	endTime := time.Date(now.Year(), now.Month(), now.Day(), end, 0, 0, 0, now.Location())

	if now.After(startTime) == true && now.Before(endTime) == true {
		return true
	}

	return false
}

func CopyMap(src map[string]interface{}) map[string]interface{} {
	dst := make(map[string]interface{})
	for k, v := range src {
		dst[k] = v
	}

	return dst
}

// rounds time with the given duration
func RoundTime(t time.Time, duration time.Duration) time.Time {
	return t.Round(duration) //.Add(-5*60*time.Minute - 30*time.Minute)
}

func RoundTimeDown(t time.Time, dur time.Duration, startMonday bool) (time.Time, error) {
	l := t.Location()
	year := t.Year()
	month := t.Month()
	day := t.Day()
	hour := t.Hour()
	min := t.Minute()
	switch dur {
	case time.Minute:
		return time.Date(year, month, day, hour, min, 0, 0, l), nil
	case time.Hour:
		return time.Date(year, month, day, hour, 0, 0, 0, l), nil
	case 24 * time.Hour:
		return time.Date(year, month, day, 0, 0, 0, 0, l), nil
	case 7 * 24 * time.Hour:
		return RoundToWeek(t, startMonday), nil
	}
	return t, errors.New("duration not implemented")
}

// rounds time to last sunday, always rounds dowm
func RoundToWeek(t time.Time, startMonday bool) time.Time {
	var day time.Time
	if startMonday {
		day = time.Date(2014, time.January, 6, 0, 0, 0, 0, t.Location())
	} else { // start sunday
		day = time.Date(2014, time.January, 5, 0, 0, 0, 0, t.Location())
	}
	//log.Println("day is", day)
	DIFF := 7 * 24 * time.Hour // 7 days
	dur := t.Sub(day)
	diff := int64(dur) / int64(DIFF)
	return day.Add(time.Duration(diff) * DIFF)
}

func AbsDiff(first, sec uint64) uint64 {
	if first > sec {
		return first - sec
	}
	return sec - first
}

func Permute(start int, input []string, output *[]string) {

	if start == len(input) {
		*output = append(*output, strings.Join(input, ""))
	}

	for i := start; i < len(input); i++ {
		input[i], input[start] = input[start], input[i]
		Permute(start+1, input, output)
		input[i], input[start] = input[start], input[i]
	}
}

func (p StringSlice) Len() int {
	return len(p)
}

func (p StringSlice) Less(i, j int) bool {
	ret := strings.ToLower(p[i]) < strings.ToLower(p[j])
	return ret
}

func (p StringSlice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func ScanForUids(c chan string, key string, pool *redigo.Pool) {
	defer close(c)
	con := pool.Get()
	defer con.Close()
	t, err := redigo.String(con.Do("TYPE", key))
	if err != nil {
		log.Println("cant get type for ", key, err.Error())
		return
	}
	cmd := "ZSCAN"
	if strings.ToLower(t) == "set" {
		cmd = "SSCAN"
	}
	cursor := 0
	for {
		reply, err := redigo.Values(con.Do(cmd, key, cursor))
		if err == nil {
			var uids []string
			skip := false
			if _, err = redigo.Scan(reply, &cursor, &uids); err == nil {
				for _, uid := range uids {
					if !skip {
						c <- uid
						//log.Debug(uid)
					}
					if cmd == "ZSCAN" {
						skip = !skip
					}
				}
				if cursor == 0 {
					break
				}
			} else {
				log.Println("error redigo.scan" + err.Error())
				break
			}
		} else {
			log.Println("error zscan " + err.Error())
			break
		}
	}
}

func DumpStack(full bool) {

	// this is only for debug, call whereever u want and change buf size appropriately
	buf := make([]byte, 1000)
	runtime.Stack(buf, full)

	log.Println("Dumping stack....", string(buf))
}

func SendMail(to []string, from, subject, content string) error {
	header := make(map[string]string)
	header["From"] = from
	header["Subject"] = subject
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = "text/plain; charset=\"utf-8\""
	header["Content-Transfer-Encoding"] = "base64"

	message := ""
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}

	message += "\r\n" + base64.StdEncoding.EncodeToString([]byte(content))

	auth := smtp.PlainAuth("", "cs@redmonstergames.com", "amazingrmg", "smtp.gmail.com")

	err := smtp.SendMail("smtp.gmail.com:587",
		auth,
		from,
		to,
		[]byte(message))

	return err
}

func ExecCmd(cmd string, wg *sync.WaitGroup) (string, error) {
	parts := strings.Fields(cmd)
	head := parts[0]
	parts = parts[1:len(parts)]

	out, err := exec.Command(head, parts...).Output()
	if wg != nil {
		wg.Done()
	}

	return string(out), err
}

// checks if the request has all required params or not
func HasRequiredParams(req *http.Request, params []string) bool {
	req.ParseForm()
	for key := range params {
		if req.PostForm[params[key]] == nil || len(req.PostForm[params[key]]) == 0 {
			return false
		}
	}
	return true
}

func RandFloat64(min float64, max float64) float64 {
	if min < 0 {
		min = 0
	}
	if max < 0 {
		max = 0
	}
	if min >= max {
		return max
	}
	return min + rand.Float64() * (max-min)
}

func trim(val string) string {
	return strings.TrimSpace(val)
}

func ParseLevelValueMapForDelim(value string, defVal interface{}, sectionSeparator, valueSeparator, keySeparator, rangeSeparator string) interface{} {
	keyValMap := make([]interface{}, 101)
	for i:=0; i<101; i++ {
		keyValMap[i] = defVal
	}
	data := trim(value)
	if data == "" {
		log.Println("KeyRange - nil value, returning default ", keyValMap)
	} else {
		// Split Dat with outer delimiter eg: 1-10,20-25:2; 30,40:1
		values := strings.Split(data, sectionSeparator)
		outerLen := len(values)
		for i:=0; i < outerLen; i++ {
			var valForRange interface{} = defVal
			innerValues := strings.Split(trim(values[i]), valueSeparator)
			if (len(innerValues) == 2) {
				valForRange, _ = strconv.ParseInt(trim(innerValues[1]), 10, 32)
				ranges := strings.Split(trim(innerValues[0]), keySeparator)
				rangesLen := len(ranges)
				for j:=0; j< rangesLen; j++ {
					if strings.Contains(ranges[j], rangeSeparator) {
						subRanges := strings.Split(trim(ranges[j]), rangeSeparator)
						if len(subRanges) == 2 {
							low, _ := strconv.ParseInt(trim(subRanges[0]), 10, 32)
							high, _ := strconv.ParseInt(trim(subRanges[1]), 10, 32)
							// swap
							if low > high {
								low = low + high
								high = low - high
								low = low - high
							}
							if low > 0 && high <= 100 {
								for k:=low; k <= high; k++ {
									keyValMap[k] = valForRange
								}
							}
						}
					} else {
						rangeKey, _ := strconv.ParseInt(trim(ranges[j]), 10, 32)
						if rangeKey > 0 && rangeKey <= 100 {
							keyValMap[rangeKey] = valForRange
						}
					}
				}
			}
		}
	}
	return keyValMap
}

func DayDiff (end, start int64) int {
	endTime := time.Unix(end, 0)
	startTime := time.Unix(start, 0)
	
	endDay := BeginningOfTheDay(endTime)
	startDay := BeginningOfTheDay(startTime)
	
	hours := int(endDay.Sub(startDay).Hours())
	
	days := int(hours/24)

	return days
}
