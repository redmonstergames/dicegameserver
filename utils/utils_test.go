package utils

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRandInt(t *testing.T) {
	var tests = []struct {
		min int
		max int
	}{
		{
			min: 10,
			max: 100,
		}, {
			min: 1,
			max: 1,
		}, {
			min: 10,
			max: 1,
		}, {
			min: 0,
			max: 0,
		}, {
			min: -1,
			max: 0,
		}, {
			min: 0,
			max: -1,
		},
	}

	for _, test := range tests {
		value := RandInt(test.min, test.max)
		min := test.min
		max := test.max
		if test.min > test.max {
			min = test.max
			max = test.min
		}
		assert.True(t, value >= min)
		assert.True(t, value <= max)
	}
}

func TestIface2Float(t *testing.T) {
	tests := []struct {
		iface interface{}
		value float64
		err   bool
	}{
		{
			iface: 10.5,
			value: 10.5,
			err:   false,
		}, {
			iface: "hello",
			value: 0,
			err:   true,
		}, {
			iface: 10,
			value: 0,
			err:   true,
		},
	}
	for _, test := range tests {
		value, err := Iface2Float(test.iface)
		if test.err == false {
			assert.Equal(t, test.value, value)
			assert.Nil(t, err)
		} else {
			assert.Equal(t, test.value, value)
			assert.NotNil(t, err)
		}
	}
}

func TestIface2UInt(t *testing.T) {
	tests := []struct {
		iface interface{}
		value uint64
		err   bool
	}{
		{
			iface: uint64(10),
			value: 10,
			err:   false,
		}, {
			iface: "hello",
			value: 0,
			err:   true,
		}, {
			iface: float64(10.4),
			value: 10,
			err:   false,
		},
	}
	for _, test := range tests {
		value, err := Iface2UInt(test.iface)
		if test.err == false {
			assert.Equal(t, test.value, value)
			assert.Nil(t, err)
		} else {
			assert.Equal(t, test.value, value)
			assert.NotNil(t, err)
		}
	}
}

func TestIface2Int(t *testing.T) {
	tests := []struct {
		iface interface{}
		value int64
		err   bool
	}{
		{
			iface: int64(10),
			value: 10,
			err:   false,
		}, {
			iface: "hello",
			value: 0,
			err:   true,
		}, {
			iface: float64(10.4),
			value: 10,
			err:   false,
		},
	}
	for _, test := range tests {
		value, err := Iface2Int(test.iface)
		if test.err == false {
			assert.Equal(t, test.value, value)
			assert.Nil(t, err)
		} else {
			assert.Equal(t, test.value, value)
			assert.NotNil(t, err)
		}
	}
}

func TestIface2String(t *testing.T) {
	tests := []struct {
		iface interface{}
		value string
		err   bool
	}{
		{
			iface: int64(10),
			value: "",
			err:   true,
		}, {
			iface: "hello",
			value: "hello",
			err:   false,
		}, {
			iface: float64(10.4),
			value: "",
			err:   true,
		},
	}
	for _, test := range tests {
		value, err := Iface2String(test.iface)
		if test.err == false {
			assert.Equal(t, test.value, value)
			assert.Nil(t, err)
		} else {
			assert.Equal(t, test.value, value)
			assert.NotNil(t, err)
		}
	}
}

func TestIface2Bool(t *testing.T) {
	tests := []struct {
		iface interface{}
		value string
		err   bool
	}{
		{
			iface: int64(10),
			value: "",
			err:   true,
		}, {
			iface: "hello",
			value: "hello",
			err:   false,
		}, {
			iface: float64(10.4),
			value: "",
			err:   true,
		},
	}
	for _, test := range tests {
		value, err := Iface2String(test.iface)
		if test.err == false {
			assert.Equal(t, test.value, value)
			assert.Nil(t, err)
		} else {
			assert.Equal(t, test.value, value)
			assert.NotNil(t, err)
		}
	}
}
