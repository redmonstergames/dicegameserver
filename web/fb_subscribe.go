package web

import (
	"fmt"
	"net/http"
	"net/url"

	"bitbucket.org/redmonstergames/stickcricketserver/log"
)

const (
	VERIFY_TOKEN = "mysecret"
)

func FbSubscribeCallback(w http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		query_params, _ := url.ParseQuery(req.URL.RawQuery)
		hubMode, okHub := query_params["hub.mode"]
		token, okToken := query_params["hub.verify_token"]
		challenge, okChallenge := query_params["hub.challenge"]

		log.Debug("Params for fbsubscribe method - ", query_params)

		if okHub && okToken && okChallenge {
			if hubMode[0] == "subscribe" && token[0] == VERIFY_TOKEN {
				log.Debug("Returning challenge....", challenge[0])
				fmt.Fprintf(w, "%s", challenge[0])
			}
		}

	} else if req.Method == "POST" {

	}
}
