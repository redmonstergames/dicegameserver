// Package stats provides all stats related calls.
//
// you can check for keys by running `scan 0 MATCH stats:* COUNT 1000`
package stats
