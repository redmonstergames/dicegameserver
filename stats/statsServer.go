package stats

import (
	"fmt"
	"github.com/gorilla/rpc/v2"
	"github.com/gorilla/rpc/v2/json2"
	"net/http"
)

type Handler func(w http.ResponseWriter, r *http.Request)

//AuthHandler validates request and passes it through only
//if it is made by a valid user
type AuthHandler func(Handler) Handler

//StatsServerConfig is the configuration required by stats server in 'Setup'
type StatsServerConfig struct {
	// auth handler for http
	Handler AuthHandler
	// the redis server we should hit
	Server string
	// if stats server should serve the http connections
	ShouldServe bool
	// host to listen for HTTP connections on
	ServerHost *string
	// port to start HTTP server on
	ServerPort *string
	//location of static files
	StaticFilesPath *string
}

const DEFAULT_STATIC_FILE_PATH = "./static/"

const (
	STATS_URL              = "/stats/"
	STATS_STATIC_FILES_URL = STATS_URL + "static/"
	STATS_LOGIN_URL        = STATS_URL + "login/"
	STATS_INDEX_URL        = STATS_STATIC_FILES_URL + "index.html"
	STATS_API_URL          = STATS_URL + "api/"
	STATS_API_V1           = "v1/"
	STATS_API_V1_URL       = STATS_API_URL + STATS_API_V1
)

const (
	HTTP_CONTENT_TYPE = "Content-Type"
	HTTP_CONTENT_TEXT = "text/plain"
	HTTP_CONTEXT_HTML = "text/html"
	HTTP_CONTENT_JSON = "application/json"
)

// store stats server config
var config *StatsServerConfig

//SetupServer initializes the statsServer with a config
func SetupServer(conf *StatsServerConfig) (err error) {
	//set config
	config = conf
	Setup(conf.Server)
	// stats http handler
	http.Handle(STATS_STATIC_FILES_URL, http.StripPrefix(STATS_STATIC_FILES_URL, http.FileServer(http.Dir(getStaticFilesPath()))))
	http.HandleFunc(STATS_URL, stats)
	http.HandleFunc(STATS_LOGIN_URL, checkAccess)

	//API requests
	s := rpc.NewServer()
	s.RegisterCodec(json2.NewCustomCodec(&rpc.CompressionSelector{}), HTTP_CONTENT_JSON)
	s.RegisterService(new(StatsService), "")
	http.Handle(STATS_API_V1_URL, s)

	fmt.Println("server setup Done")

	return nil
}

//StartServer starts serving HTTP conneciton from stats server
//only if 'shouldServe' was set in config
func StartServer() (err error) {
	if config.ShouldServe {
		if len(*config.ServerHost) > 0 && len(*config.ServerPort) > 0 {
			err = http.ListenAndServe(*config.ServerHost+":"+*config.ServerPort, nil)
		}
	}
	return err
}

func getStaticFilesPath() string {
	if config.StaticFilesPath != nil && len(*config.StaticFilesPath) > 0 {
		return *config.StaticFilesPath
	}
	return DEFAULT_STATIC_FILE_PATH
}

func stats(w http.ResponseWriter, req *http.Request) {
	http.Redirect(w, req, STATS_INDEX_URL, http.StatusMovedPermanently)
}

func checkAccess(w http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" {
		http.Error(w, "Method nod allowed", http.StatusMethodNotAllowed)
		return
	}

	w.Header().Set(HTTP_CONTENT_TYPE, HTTP_CONTENT_TEXT)
	fmt.Fprintf(w, "%s", "It Works!")
}
