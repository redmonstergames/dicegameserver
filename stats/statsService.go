package stats

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/garyburd/redigo/redis"
)

type StatsService struct{}

type ConcurrentRequest struct {
	Key        string `json:"key"`   // key to fetch concurrent for
	RangeStart int64  `json:"start"` // start time stamp for sample
	RangeEnd   int64  `json:"end"`   // end time stam for sample
}

type ConcurrentReply struct {
	Data string `json:"data"`
}

const (
	CSV_DELIM   = ", "
	CSV_NEWLINE = "$"
)

// test query ->
// curl -X POST -H "Content-Type: application/json" -d '{"method":"StatsService.GetConcurrent","params":{"key":"Connect", "start":1408881825, "end":1408999825}, "id":"10", "jsonrpc": "2.0"}' http://127.0.0.1:8080/stats/api/v1/
func (ss *StatsService) GetConcurrent(r *http.Request, req *ConcurrentRequest, reply *ConcurrentReply) error {
	now := getISTTime()
	end := convertToISTTime(time.Unix(req.RangeEnd, 0))
	start := convertToISTTime(time.Unix(req.RangeStart, 0))
	if req.RangeEnd == 0 || now.Sub(end) < 0 {
		end = now
	}
	if req.RangeStart == 0 {
		start = now
	}
	diff := end.Sub(start)
	if diff < 0 {
		return errors.New("start time can not be greater then end time")
	}
	sec := time.Now()
	slice := getTimeSlice()
	count := int64(diff / slice)
	fmt.Println("fetching redis conn with count = ", count)
	c := redisConnPool.Get()
	reply.Data = ""
	for i := int64(0); i <= count; i++ {
		t := start.Add(slice * time.Duration(i))
		key := GetConcurrentKeyWithTime(req.Key, t)
		c.Send("GET", key)
	}
	//fmt.Println("looped through all ", count, "keys")
	c.Flush()
	for i := int64(0); i <= count; i++ {
		t := start.Add(slice * time.Duration(i))
		key := GetConcurrentKeyWithTime(req.Key, t)
		n, err := redis.Int64(c.Receive())
		if err != nil {
			if err == redis.ErrNil {
				n = 0
			} else {
				//do error log
				fmt.Println("error ", err.Error())
				break
			}
		}
		reply.Data += key + CSV_DELIM + strconv.FormatInt(t.Unix(), 10) + CSV_DELIM + strconv.FormatInt(n, 10) + "\n"
		//fmt.Println(key + CSV_DELIM + strconv.FormatInt(t.Unix(), 10) + CSV_DELIM + strconv.FormatInt(n, 10))
	}
	fmt.Println("took ", time.Now().Sub(sec), " secs to finish")
	return nil
}

type DAURequest struct {
	RangeStart int64 `json:"start"` // start time stamp for sample
	RangeEnd   int64 `json:"end"`   // end time stamp for sample
}

type DAUReply struct {
	Data string `json:"data"`
}

func (ss *StatsService) GetDAU(r *http.Request, req *DAURequest, reply *DAUReply) error {
	now := getISTTime()
	end := convertToISTTime(time.Unix(req.RangeEnd, 0))
	start := convertToISTTime(time.Unix(req.RangeStart, 0))
	if req.RangeEnd == 0 || now.Sub(end) < 0 {
		end = now
	}
	if req.RangeStart == 0 {
		start = now
	}
	diff := end.Sub(start)
	if diff < 0 {
		return errors.New("start time can not be greater then end time")
	}
	sec := time.Now()
	slice := time.Hour * 24
	count := int64(diff / slice)
	fmt.Println("fetching redis conn with count = ", count)
	c := redisConnPool.Get()
	reply.Data = ""
	for i := int64(0); i <= count; i++ {
		t := start.Add(slice * time.Duration(i))
		key := GetDAUKeyWithTime(t)
		c.Send("SCARD", key)
	}
	//fmt.Println("looped through all ", count, "keys")
	c.Flush()
	for i := int64(0); i <= count; i++ {
		t := start.Add(slice * time.Duration(i))
		key := GetDAUKeyWithTime(t)
		n, err := redis.Int64(c.Receive())
		if err != nil {
			if err == redis.ErrNil {
				n = 0
			} else {
				//do error log
				fmt.Println("error ", err.Error())
				break
			}
		}
		reply.Data += key + CSV_DELIM + strconv.FormatInt(t.Unix(), 10) + CSV_DELIM + strconv.FormatInt(n, 10) + CSV_NEWLINE
		//fmt.Println(key + CSV_DELIM + strconv.FormatInt(t.Unix(), 10) + CSV_DELIM + strconv.FormatInt(n, 10))
	}
	fmt.Println("took ", time.Now().Sub(sec), " secs to finish")
	return nil
}
