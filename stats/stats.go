package stats

import (
	"bitbucket.org/redmonstergames/stickcricketserver/redisPool"
	"errors"
	"time"

	"github.com/garyburd/redigo/redis"
)

const LOCATION = "Asia/Kolkata"
const DATE_FORMAT = "02-Jan-2006"
const DATE_TIME_FORMAT = "02-Jan-2006-15-04"

const STATS_PREFIX = "stats"
const DAU_PREFIX = "dau"
const SESSION_PREFIX = "session"
const CONCURRENT_PREFIX = "con"
const SET_PREFIX = "set"
const COUNT_PREFIX = "count"
const DELIM = ":"

const (
	ENGAGEMENT_HIGH = "high" // >= 6  days in last 7 days
	ENGAGEMENT_MED  = "med"  // [4-6) days in last 7 days
	ENGAGEMENT_LOW  = "low"  // < 4 days in last 7 days
)

//time slice in minutes
const CONCURRENT_TIME_SLICE = 5

// redis connectiong pool
var redisConnPool *redis.Pool

// setup a client connection to stats redis server
func Setup(server string) {
	redisConnPool = redisPool.GetPool(server)
}

func GetSetValues(key string) ([]string, error) {
	if redisConnPool == nil {
		return []string{}, errors.New("Connection not initialized")
	}
	conn := redisConnPool.Get()
	defer conn.Close()
	conn.Send("SMEMBERS", key)
	conn.Flush()

	reply, err := redis.Strings(redis.MultiBulk(conn.Receive()))
	if err != nil {
		return []string{}, err
	}
	return reply, err
}

// returns time in IST
func getISTTime() time.Time {
	t := time.Now()
	return convertToISTTime(t)
}

func convertToISTTime(t time.Time) time.Time {
	l, err := time.LoadLocation(LOCATION)

	if err == nil && l != nil {
		t = t.In(l)
	} else { // do it manually
		l, err = time.LoadLocation("UTC")

		if err == nil {
			t = t.In(l)

			istDiff := int64(60 * 60 * 5.5)
			t = t.Add(time.Duration(istDiff) * time.Second)
		}
	}

	return t
}

// get the current date
func getCurrentDate() string {
	return getDateWithDiff(0)
}

// get date with duration
func getDateWithDiff(dur time.Duration) string {
	t := getISTTime()
	t = t.Add(dur)
	return getDateWithTime(t)
}

func getDateWithTime(t time.Time) string {
	return t.Format(DATE_FORMAT)
}

// gets the time slice value in mins for concurrent tracking
func getTimeSlice() time.Duration {
	return CONCURRENT_TIME_SLICE * time.Minute
}

// gets the time slot in which we need to place this concurrent
func GetConcurrentSlot() string {
	return GetSlotWithDiff(0)
}

func GetSlotWithDiff(dur time.Duration) string {
	t := getISTTime()
	t = t.Add(dur)
	return GetSlotWithTime(t)
}

func GetSlotWithTime(t time.Time) string {
	// we need to round off for the given duration
	duration := -getTimeSlice() / 2
	t = t.Add(duration)
	return t.Round(getTimeSlice()).Format(DATE_TIME_FORMAT)
}

func GetDAUKeyBase() string {
	return STATS_PREFIX + DELIM + DAU_PREFIX + DELIM
}

// get DAU tracking key for current day
// sample key stats:dau:21-Aug-2014
func GetDAUKey() string {
	return GetDAUKeyWithDiff(0)
}

func GetDAUKeyWithDiff(dur time.Duration) string {
	return GetDAUKeyBase() + getDateWithDiff(dur)
}

func GetDAUKeyWithTime(t time.Time) string {
	return GetDAUKeyBase() + getDateWithTime(t)
}

func GetCountKey(key string) string {
	return GetCountKeyWithDiff(key, 0)
}

func GetCountKeyWithDiff(key string, dur time.Duration) string {
	return STATS_PREFIX + DELIM + COUNT_PREFIX + DELIM + key + DELIM + getDateWithDiff(dur)
}

// get session tracking key for current day
// sample key stats:session:21-Aug-2014
func GetSessionKey() string {
	return GetSessionKeyWithDiff(0)
}

func GetSessionKeyWithDiff(dur time.Duration) string {
	return STATS_PREFIX + DELIM + SESSION_PREFIX + DELIM + getDateWithDiff(dur)
}

// get concurrent tracking key for current duration
// sample key -> stats:con:Join:21-Aug-2014-01-10
func GetConcurrentKey(key string) string {
	return GetConcurrentKeyWithDiff(key, 0)
}

func getConcurrentKeyBase(key string) string {
	return STATS_PREFIX + DELIM + CONCURRENT_PREFIX + DELIM + key + DELIM
}

func GetConcurrentKeyWithDiff(key string, dur time.Duration) string {
	return getConcurrentKeyBase(key) + GetSlotWithDiff(dur)
}

func GetConcurrentKeyWithTime(key string, t time.Time) string {
	return getConcurrentKeyBase(key) + GetSlotWithTime(t)
}

// get set tracking key for the current day
// sample key -> stats:set:New:21-Aug-2014
func GetSetKey(key string) string {
	return GetSetKeyWithDiff(key, 0)
}

func GetSetKeyWithDiff(key string, dur time.Duration) string {
	return STATS_PREFIX + DELIM + SET_PREFIX + DELIM + key + DELIM + GetDAUKeyWithDiff(dur)
}

//RecordLogin records login for user
func RecordLogin(uid string) (err error) {
	if redisConnPool == nil {
		return errors.New("Connection not initialized")
	}
	key := GetDAUKey()
	conn := redisConnPool.Get()
	defer conn.Close()
	_, err = conn.Do("SADD", key, uid)
	//_, err = statsRedisConnection.SAdd(key, uid)
	return err
}

//RecordSession records session for current day
func RecordSession() (err error) {
	if redisConnPool == nil {
		return errors.New("Connection not initialized")
	}
	conn := redisConnPool.Get()
	defer conn.Close()
	key := GetSessionKey()
	_, err = conn.Do("INCR", key)
	return err
}

//RecordConcurrent records concurrent for a given key
func RecordConcurrent(key string) (err error) {
	if redisConnPool == nil {
		return errors.New("Connection not initialized")
	}
	if len(key) == 0 {
		return errors.New("key length cant be 0")
	}
	conn := redisConnPool.Get()
	defer conn.Close()
	redisKey := GetConcurrentKey(key)
	_, err = conn.Do("INCR", redisKey)
	//_, err = statsRedisConnection.Incr(redisKey)
	return err
}

//RecordSet sets the given uid in a Set created by the given key
func RecordSet(key, uid string) (err error) {
	if redisConnPool == nil {
		return errors.New("Connection not initialized")
	}
	if len(key) == 0 {
		return errors.New("key length cant be 0")
	}
	if len(uid) == 0 {
		return errors.New("uid length cant be 0")
	}
	redisKey := GetSetKey(key)
	conn := redisConnPool.Get()
	defer conn.Close()
	_, err = conn.Do("SADD", redisKey, uid)
	//_, err = statsRedisConnection.SAdd(redisKey, uid)
	return err
}

func RecordCount(key string) (err error) {
	if redisConnPool == nil {
		return errors.New("Connection not initialized")
	}
	conn := redisConnPool.Get()
	defer conn.Close()
	countKey := GetCountKey(key)
	_, err = conn.Do("INCR", countKey)
	return err
}

//GetEngagement fetches the engagement for the user
// returns ENGAGEMENT_* values
func GetEngagement(uid string) (string, error) {
	if redisConnPool == nil {
		return "", errors.New("Connection not initialized")
	}
	conn := redisConnPool.Get() // get connection from pool
	// queue SISMEMBER commands
	for i := 0; i < 7; i++ {
		dur := -24 * time.Duration(i) * time.Hour
		conn.Send("SISMEMBER", GetDAUKeyWithDiff(dur), uid)
	}
	// make the request
	conn.Flush()
	// count occurances
	sessions := 0
	for i := 0; i < 7; i++ {
		n, err := conn.Receive()
		value, err := redis.Int(n, err)
		if err != nil {
			return "", err
		}
		sessions += value
	}
	// return engagement
	if sessions >= 6 {
		return ENGAGEMENT_HIGH, nil
	} else if sessions < 4 {
		return ENGAGEMENT_LOW, nil
	} else {
		return ENGAGEMENT_MED, nil
	}
}
