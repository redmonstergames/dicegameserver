## Real Teen Patti Server

This repo tracks code for Real Teen Patti's Server

### Dependencies
* need to have [Go](http://golang.org/doc/install) installed on your machine to develop code
* need to have a running instance of [Redis](http://redis.io/)

### Development setup
* create the repo folder `mkdir -p $GOPATH/src/bitbucket.org/kiran_inbng/rmg_teenpattiserver`
* go to the created folder `cd $GOPATH/src/bitbucket.org/kiran_inbng/rmg_teenpattiserver`
* clone the repo `git clone git@bitbucket.org:kiran_inbng/rmg_teenpattiserver.git .`
* install [godep](https://github.com/tools/godep) `go get github.com/tools/godep`
* restore dependencies `godep restore`
* build package `go build` or use `make all`
* run server `go run tpsrv.go` or use `make run`

#### Docker Setup
* Make sure you have [Docker](https://docs.docker.com/installation/#installation) installed
* if using [Boot2Docker](http://boot2docker.io/) run `boot2docker start` and use the IP provided by `boot2docker ip`
* install [Fig](http://www.fig.sh/)
* run `fig up` - this will spin up game server (8080), admin server(9090) and redis server(6379)

### Overrides
create the following override file in 'config/ServerFlags.override.json'

    {
      "log_level": 5,
      "MRT_BUCKET_NAME":"com.rmg.realteenpatti.mrt.dev",
      "MCONFIG_BUCKET_NAME":"com.rmg.realteenpatti.mconfig.dev"
    }

this will switch packages 'mrt' and 'mconfig' to dev config and enable debug logs

please do not modify config directly in 'config/ServerFlags.json' or 'config/RuntimeFlags.json' for testing changes,
always use 'config/ServerFlags.override.json' or 'config/RuntimeFlags.override.json'

#### How Do Overrides Work ?
all the entries in RuntimeFlags.json are sent to Client(App) and all entires in ServerFlags.json are for server use,
overrides are loaded by runtime package on first run and on Reload()

the order in which overrides are read is as follows

* RuntimeFlags.json -- commited to repo only prod values
* ServerFlags.json -- commited to repo only prod values
* MRT -- specific environment for dev/stage/prod
* RuntimeFlags.override.json -- always used to override NEVER commited to repo
* ServerFlags.override.json -- always used to override NEVER commited to repo

### Make
the following make args are available

* `make all` or `make build` - cleans and builds server/admin/other packages checking for any build failure
* `make adminserver` - builds adminserver
* `make rmg_teenpattiserver` - builds main real teen patti server
* `make clean` - cleans go builds and removes binary files
* `make run` - builds and runs rmg server with default(8080) config
* `make run PORT=8282` - builds and runs rmg server with port : 8282 and default config
* `make runadmin` - builds and runs rmg admin sever with default(9090) config
* `make reload` - issues HUP to running rmg\_teenpattiserver forcing server to reload config
* `make index` - starts godoc server on 6060

### Packages
Below are the packages that this repo contains

#### tpgame
Main package containing all of game logic

#### stats
Provides all stats related calls

#### reports
Provides all reports related function like CURR/NURR/RURR and Retention and relies on [MongoDB](http://www.mongodb.org/)
[Aggregation](http://docs.mongodb.org/manual/core/aggregation-introduction/)

### Contact/Owner
* [Kiran](mailto:kiran@redmonstergames.com)

