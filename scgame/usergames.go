package scgame
import (
	"fmt"
	"errors"
	"bitbucket.org/redmonstergames/stickcricketserver/log"
	"bitbucket.org/redmonstergames/stickcricketserver/utils"
//	"bitbucket.org/redmonstergames/stickcricketserver/json"

)
var ErrorMatchesNotFound = errors.New("MatchesnotFound")

func GetUserGames(uid string) (userData []string , err error){
	userData ,err = rcUserGamesList.SMembers(GetuserGameKey(uid))
	log.Info("user matches found are",userData , GetuserGameKey(uid))
	if err == nil && len(userData) > 0 {
		utils.LogOnError(err)
		return userData , nil
	}
	log.Info("Get Match error - ", uid, err, userData)
	return userData , ErrorMatchesNotFound
}

func GetuserGameKey(uid string) string {

     key := fmt.Sprintf("%s:%s", USER_GAME_PREFIX,uid)
	return key


}
func GetallGameData(uid string) (gameslist *GamesList){
	var Err error
	var GidList []string
	GidList,Err = GetUserGames(uid)
	if(Err == ErrorMatchesNotFound || len(GidList) == 0 ){
		  gameslist =  &GamesList{}
	      return gameslist
	}
	gameslist = new(GamesList)
	for  _ , gid := range GidList {
		var matchFound *Game
		matchFound,Err = GetGame(gid)
		if(matchFound != nil && Err == nil){
			gameslist.Add(matchFound)
		}else if(matchFound == nil && Err != nil){

			log.Info("Error while retreving match from game id" , gid)
		}

		//TODO:handle  error pending
	}
        return gameslist
}







