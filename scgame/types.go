package scgame

import (
	"sync"
	//"time"

	//"github.com/gorilla/websocket"
)

type UserProfile struct {
	Uid           string                `json:"uid"`
	SnUid         string                `json:"snuid"`
	Nid           uint64                `json:"nid"`
	FirstName     string                `json:"na"`
	LastName      string                `json:"lna"`
	Pic           string                `json:"pic"`
	Age           string                `json:"ag"`
	Location      string                `json:"lo"`
	Gender        string                `json:"ge"`
	Birthday      string                `json:"bd"`
	Email         string                `json:"em"`
	Locale        string                `json:"lcl"`
}

type DeviceData struct {
	LastLogin uint64 `json:"ll"`
	Token     string `json:"token"`
}


type User struct {
	UserProfile
	CreatedTime               uint64             `json:"ct"`
	Traits                    uint64             `json:"ts"`
	TotalNumberOfGamesPlayed  uint64             `json:"tngp"`
	NumberofGamesWon          uint64              `json:"ngw"`
	BestScore                 uint64              `json:"bs"`
	TypeOfDice                uint64              `json:"tod"`
	Level                     uint64              `json:"lvl"`
	XPLevel                   uint64              `json:"xlvl"`
	ReplyTime                 uint64               `json:"rt"`
	ProfilePicUrl             string              `json:"url"`
	lock                      sync.Mutex
}

type GamesList struct {
	lock  sync.RWMutex
	Games []*Game  `json:"mths"`
}


type PlayerStats struct {
	chipsGameStart float64
	chipsStart     float64
	ChipsWon       float64 `json:"pcw"`
	ChipsLost      float64 `json:"pcl"`
	GamesPlayed    uint64  `json:"pgp"`
	GamesWon       uint64  `json:"pgw"`
}

type UserShort struct {
	Uid             string  `json:"uid"`
	Name            string  `json:"na"`
	Pic             string  `json:"pi"`
	Level           uint64  `json:"lvl"`
	ChipsCur        float64 `json:"cc"`
	GamesPlayed     uint64  `json:"gp"`
	GamesWon        uint64  `json:"gw"`
	Title           string  `json:"title"`
	LeaderboardRank uint64  `json:"lbr"`
	Xp              float64 `json:"xp"`
	Cash            float64 `json:"cash"`
}


type Game struct {
	GameDetails        *SessionDetails 	`json:"sd"`
	SenderPlayer        *Player        	`json:"sp"`
	RecipientPlayer     *Player   		`json:"rp"`
	GameType            uint64   		`json:"gt"`
	CurrentPlayerID     string   		`json:"cuid"`
	IsInvitaionAccepted bool    		`json:"bisAcc"`
	WinnerName          string   		`json:"wn"`

  }
type SessionDetails struct {
	Gid    string  `json:"sid"`
	SessionDate uint64  `json:"sd"`
}

type Player struct {
	Uid            string     	`json:"uid"`
	AreTurnsOver   bool       	`json:"bto"`
	Name           string      	`json:"nm"`
	TurnsLeft      uint64       `json:"tl"`
	GameDetails    *SessionDetails `json:"sd"`
	scoreboard     *ScoreBoard 	`json:"sb"`
	lock           sync.Mutex
}

type ScoreBoard struct {
	Variations []string   `json:"v"`
	Score      []string    `json:"s"`
}


type GameManager struct {
	gamesAvailable map[string]*Game
	gamesFull      map[string]*Game
	free           chan *Game
	full           chan *Game
	over           chan *Game
	lock           sync.Mutex
}

type Message map[string]interface{}

type GameMessage struct {
	Msg      uint64
	Data     interface{}
	sig      string
}

const (
	USER_HASH_KEY_UID = "uid"
	GAME_HASH_KEY_UID = "gid"
)

const (
	PLAYER_STATE_UNKNOWN = iota
	PLAYER_STATE_PLAYING
	PLAYER_STATE_SPECTATOR
	PLAYER_STATE_JOINING
	PLAYER_STATE_FINISHEDPLAY
	PLAYER_STATE_LEFT
)

const (
	GAME_STATE_NOTSTARTED = iota
	GAME_STATE_WAITING
	GAME_STATE_STARTING
	GAME_STATE_PROGRESS
	GAME_STATE_CLOSING
)

const (
	MESSAGE_SUCCESS = iota //0
	MESSAGE_ERROR
	MESSAGE_NO_USER
	MESSAGE_USER_FOUND
	MESSAGE_USER_CREATED
	MESSAGE_USER_EXISTS
	MESSAGE_GAME_INFO
	MESSAGE_JOIN_GAME //6
	MESSAGE_INVALID_SESSION
	MESSAGE_CREATE_GAME_ERROR
	MESSAGE_INVALID_GAME_ID
	MESSAGE_GAME_FULL
	MESSAGE_PLAYERS_DATA
	MESSAGE_SCORE_UPDATE
	MESSSAGE_USER_UPDATE

)

const (
	BOT_TYPE_AGG = iota
	BOT_TYPE_BAL
	BOT_TYPE_WUSS
	BOT_TYPE_BAD
	BOT_TYPE_EVIL
	BOT_TYPE_SCRIPTED
)

const (
	PLAYER_TYPE_REAL = iota
	PLAYER_TYPE_BOT
)

const (
	NETWORK_ANON = iota
	NETWORK_FB
)




type FTUEState uint64

const (
	FTUE_STATE_NONE = FTUEState(iota)
	FTUE_STATE_DONE
)