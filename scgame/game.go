package scgame
import (
	"fmt"
	"bitbucket.org/redmonstergames/stickcricketserver/json"
	"bitbucket.org/redmonstergames/stickcricketserver/log"
	"bitbucket.org/redmonstergames/stickcricketserver/utils"
)


func NewGame( ) (game *Game){

	game = new(Game)
	game.CurrentPlayerID = ""
	game.GameType = uint64(0)
	game.GameDetails = &SessionDetails{"",uint64(0)}
	game.IsInvitaionAccepted = false
	game.RecipientPlayer = &Player{}
	game.SenderPlayer = &Player{}
	game.WinnerName = ""

	return game
}


func (game *Game) CreateGame() error {
	gameData, err := json.Marshal(game)
	log.Debug("user create err - ", game, err)
	//utils.QuitOnError(err)
	err = rcGame.SetWithNx(GetGameKey(game.GameDetails.Gid), string(gameData))
	return err
}

func GetGameKey(gid string) string {

	key := fmt.Sprintf("%s:%s", GAME_KEY_PREFIX, gid)
	return key


}
func GetGame(gid string) (game *Game, err error) {
	var gameData string
	gameData, err = rcGame.Get(GetGameKey(gid))
	log.Info("match key generated is", GetGameKey(gid),"  ", gameData)
	if err == nil && gameData != "" {
		game = new(Game)
		err = json.Unmarshal([]byte(gameData), game)
		utils.LogOnError(err)
		return game, err
	}
	return nil, ErrorMatchesNotFound
}


func (game *Game) SaveGame() error{

	gameData, err := json.Marshal(game)
	utils.QuitOnError(err)

	err = rcUser.Set(GetGameKey(game.GameDetails.Gid), string(gameData))
	if err != nil {
		log.Info("error recieved: ", err.Error())
	}
	return err

}

