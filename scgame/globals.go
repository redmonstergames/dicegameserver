package scgame

import (
	//lb "bitbucket.org/redmonstergames/stickcricketserver/leaderboard"
	"github.com/kiraninbng/go-redis/redis"
	"github.com/pmylund/go-cache"
)

var rcPresense *redis.Client
var rcVolatile *redis.Client
var rcUser *redis.Client
var rcUserList *redis.Client
var rcUserGamesList *redis.Client
var rcGame * redis.Client

var gm *GameManager


var mCache *cache.Cache

func GetUserlistRedis() *redis.Client {
	return rcUserList
}

func GetVolatileRedis() *redis.Client {
	return rcVolatile
}

func GetConfigFolderPath() string {
	return *configPath
}


