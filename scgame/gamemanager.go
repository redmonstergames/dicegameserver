package scgame


import (
	"bitbucket.org/redmonstergames/stickcricketserver/utils"
	 uuid "github.com/nu7hatch/gouuid"
)



func getNewGameId() (string, error) {
	//u5, err := uuid.NewV5(uuid.NamespaceURL, []byte("rmg/teenpatti"))
	gid, err := uuid.NewV4()
	utils.LogOnError(err)

	return gid.String(), err
}


func CreategameforLevel() (game *Game,err error){

	game = NewGame()
	game.GameDetails.Gid,err = getNewGameId()
	return game,err

	//TODO;complete it when needed

}
