package scgame

import (
	"bitbucket.org/redmonstergames/stickcricketserver/json"
	"fmt"
	"net/http"

	"bitbucket.org/redmonstergames/stickcricketserver/log"
)

func sendMessage(w http.ResponseWriter, msg GameMessage) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	data, _ := json.Marshal(msg)
	fmt.Fprintf(w, "%s", data)
	log.Debug("Sending message - %s" + string(data))
}

func constructMessage(msgType uint64, data interface{}) GameMessage {
	var msg GameMessage
	msg = GameMessage{Msg: msgType, Data: data}
	return msg
}
