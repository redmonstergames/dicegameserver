package scgame

import (
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"
	"bitbucket.org/redmonstergames/stickcricketserver/log"
	"bitbucket.org/redmonstergames/stickcricketserver/utils"
	//"bitbucket.org/kiran_inbng/rmg_teenpattiserver/json"
)

var ServerPort = flag.Int("s", 8181, "server port")
var PsList = flag.String("ps", "", "presense servers")
var VolList = flag.String("vl", "", "volatile servers")
var UserList = flag.String("us", "", "user servers")
var UlsList = flag.String("uls", "", "user list servers")
var AdminList = flag.String("al", "", "Admin server")
var FluentList = flag.String("fl", "", "FluentD server")
var LeaderboardList = flag.String("lb", "", "Leaderboard server")
var IsAdmin = flag.Bool("ia", false, "Is Admin")

//var LogLevel = flag.Int("ll", 1, "0 - 5, 5 is all")
var configPath = flag.String("c", CONFIG_FOLDER_PATH, "path of config folder")
var statsList = flag.String("sl", "", "Stats reids servers")

var (
	mrtEnabled bool
)

func Start() {
	rand.Seed(time.Now().UTC().UnixNano())
	flag.Parse()
	log.Info("Starting tpgame server...")
	Setup()
	http.HandleFunc("/connect", Connect)
	http.HandleFunc("/getgamesinfo", GetGamesInfo)
	err := http.ListenAndServe(fmt.Sprintf(":%d", *ServerPort), nil)
	utils.QuitOnError(err)
}


func Connect(w http.ResponseWriter, req *http.Request) {

	log.Info("Connecting called")
	if req.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	for k, v := range req.PostForm {
		log.Debug("K- : ", k, "V :", v)
	}

	var msg GameMessage
	//required params for this request
	var requiredParams = []string{ "nid", "snuid","udid", "token"}
	 // check if we have all required values, if not error out
	if utils.HasRequiredParams(req, requiredParams) == false {
		msg = constructMessage(MESSAGE_ERROR,"no params")
		log.Info("message error has ocurr")
		sendMessage(w, msg)
		return
	}
	snuid := req.PostForm["snuid"]
	nid := req.PostForm["nid"]
	udid := req.PostForm["udid"]
//	deviceToken := req.PostForm["devicetoken"]
	fbToken := req.PostForm["token"]
//	ver, okVer := req.PostForm["ver"]
//	platform := req.FormValue("platform")
//	create := req.FormValue("create")
	network, _ := strconv.ParseUint(nid[0], 10, 64)

	if snuid[0] != "" && nid[0] != "" && udid[0] != "" && fbToken[0] != "" {
		GenUid := BuildUid(nid[0], snuid[0])
		var user *User
		var err error
		if network == NETWORK_FB {
			if validateFBToken(fbToken[0], snuid[0], GenUid) == false {
				msg = constructMessage(MESSAGE_INVALID_SESSION, "")
				sendMessage(w, msg)
				return
			}
		}
		user, err = GetUser(GenUid)

		if ( user != nil && err == nil ) {
			msg = constructMessage(MESSAGE_USER_FOUND, user)
			sendMessage(w, msg)
			return
		}
		if (err != nil) {
			user, err := InitializeNewUser(w, req)

			if (user != nil && err == nil) {

				log.Info("user created successfully")
				msg = constructMessage(MESSAGE_USER_CREATED, user)
				sendMessage(w, msg)
				return
			}else if err != nil {
					msg = constructMessage(MESSAGE_ERROR,"create error")
					sendMessage(w, msg)
					return
				}
			}

	}else{

		log.Info("Error in Connecting")
		msg = constructMessage(MESSAGE_ERROR,"")
	}

	sendMessage(w,msg)
}

func Join(w http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

}
func GetGamesInfo(w http.ResponseWriter, req *http.Request){
	log.Info("Getgame info called")
	if req.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	var msg GameMessage
	var matcheslist *GamesList
	var requiredParams = []string{"uid"}
	if utils.HasRequiredParams(req, requiredParams) == false {
		msg = constructMessage(MESSAGE_ERROR,"no params")
		log.Info("message error has ocurr")
		sendMessage(w, msg)
		return
	}
	uid := req.PostForm["uid"]
    matcheslist = GetallGameData(uid[0])
	msg = constructMessage(MESSAGE_GAME_INFO, matcheslist)
	sendMessage(w, msg)
	return

}

