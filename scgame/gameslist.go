package scgame
import "bitbucket.org/kiran_inbng/rmg_teenpattiserver/log"


func(gameslist *GamesList) Add (game *Game){

	gameslist.lock.Lock()
	defer gameslist.lock.Unlock()
	log.Info("adding match is", game.GameDetails.Gid)
	gameslist.Games = append(gameslist.Games, game)

}
