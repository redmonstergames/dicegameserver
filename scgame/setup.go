package scgame

import (
	"strconv"
	"strings"
	"time"
	"bitbucket.org/redmonstergames/stickcricketserver/log"
	"github.com/kiraninbng/go-redis/redis"
	"github.com/pmylund/go-cache"
)

func Setup() {
	setupServers()

	//	// setup mrt
	//	// mconfig
	//	mconfigEnabled, _ = utils.Iface2Bool(runtime.GetServerValue("MCONFIG_ENABLE"))
	//	if mconfigEnabled {
	//		setupMConfig()
	//	}

	// config stats


	// config event queue


	mCache = cache.New(5*time.Minute, 30*time.Second)

	checkRedisCons()
}

func setupServers() {
	if *PsList != "" {
		PresenseServers = strings.Split(*PsList, ",")
	}

	if *VolList != "" {
		VolatileServers = strings.Split(*VolList, ",")
	}

	if *UserList != "" {
		UserServers = strings.Split(*UserList, ",")
	}

	if *UlsList != "" {
		UserListServers = strings.Split(*UlsList, ",")
	}

	if *AdminList != "" {
		AdminServer = *AdminList
	}

	if *FluentList != "" {
		FluentServer = *FluentList
	}

	if *statsList != "" {
		StatsListServers = strings.Split(*statsList, ",")
	} else {
		// default to UserListServers
		StatsListServers = UserListServers
	}

	if *LeaderboardList != "" {
		LeaderboradServers = strings.Split(*LeaderboardList, ",")
	}

	rcPresense = redis.New(PresenseServers...)
	rcVolatile = redis.New(VolatileServers...)
	rcUser = redis.New(UserServers...)
	rcUserGamesList = redis.New(UserGameServers...)
	rcUserGamesList.Timeout = time.Duration(5000) * time.Millisecond
	rcGame = redis.New(GameServers...)
	rcGame.Timeout = time.Duration(5000) * time.Millisecond
	rcUser.Timeout = time.Duration(5000) * time.Millisecond
	rcUserList = redis.New(UserListServers...)
	//rcStats = redis.New(StatsListServers...)
}


//func setupMConfig() {
//	access_key, _ := utils.Iface2String(runtime.GetServerValue("MRT_ACCESS_KEY"))
//	access_secret, _ := utils.Iface2String(runtime.GetServerValue("MRT_SECRET_KEY"))
//	bucket_name, _ := utils.Iface2String(runtime.GetServerValue("MCONFIG_BUCKET_NAME"))
//	config := new(mconfig.MConfig)
//	config.BucketName = bucket_name
//	config.AWSAccessKey = access_key
//	config.AWSAccessSecret = access_secret
//	config.RefershDuration = time.Minute * 2
//	config.BucketPath = "/mconfig/"
//	config.NoLoop = false
//	config.NoFetchInSetup = false
////	config.Handler = mConfigUpdateHandler{}
//	mconfig.Setup(config)
//}
func checkRedisCons() {
	if *IsAdmin == true {
		return
	}

	log.Info("checking redis connections..")

	curTime := strconv.FormatInt(time.Now().Unix(), 10)

	updateLastStarted(rcPresense, curTime)
	log.Debug("update last started time - ", rcPresense)

	updateLastStarted(rcUser, curTime)
	log.Debug("update last started time - ", rcUser)

	updateLastStarted(rcUserList, curTime)
	log.Debug("update last started time - ", rcUserList)

	updateLastStarted(rcVolatile, curTime)
	log.Debug("update last started time - ", rcVolatile)

	updateLastStarted(rcUserGamesList, curTime)
	updateLastStarted(rcGame, curTime)
}
