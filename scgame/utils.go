package scgame

import (
	"bitbucket.org/redmonstergames/stickcricketserver/json"
	"bitbucket.org/redmonstergames/stickcricketserver/log"
	"bitbucket.org/redmonstergames/stickcricketserver/utils"

	"github.com/kiraninbng/go-redis/redis"
	//	"reflect"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"net/url"
	"sort"
	"strconv"
	"strings"

	fb "github.com/huandu/facebook"
)

type Pair struct {
	first  int
	second int
}

func (r Message) String() (s string) {
	b, err := json.Marshal(r)
	if err != nil {
		s = ""
		return
	}
	s = string(b)
	return
}


func SnuidToUid(snid, snuid string) string {
	return BuildUid(snid, snuid)
}

func BuildUid(nid string, snuid string) string {
	return GenerateUid(nid + "_" + snuid)
}

func GenerateUidFromFBId(fbid string) string {
	return GenerateUid("1_" + fbid)
}

func GenerateUid(data string) string {
	h := sha256.New()
	h.Write([]byte(data))
	uidBytes := h.Sum(nil)

	var a uint64

	l := 6
	for i := 0; i < l; i++ {
		shift := uint32((l - i - 1) * 8)

		a |= uint64(uidBytes[i]) << shift
	}

	uid := strconv.FormatUint(a, 10)
	log.Debug("Generated uid - ", uid)

	return uid
}

//func ConstructLevelData(level int) []LevelData {
//	// doing this because int does not work with json marshall at this point
//	var levelData []LevelData
//
//	if level > 0 && level <= MAX_LEVELS {
//
//		levelData = append(levelData, levelConfig[level])
//		if level+1 <= MAX_LEVELS {
//			levelData = append(levelData, levelConfig[level+1])
//		}
//	}
//
//	return levelData
//}

func GetHash(values url.Values) string {

	udid, ok := values["udid"]

	if ok == false || udid[0] == "" {
		return ""
	}

	key := udid[0] + "RMG RealTeenPatti MultiPlayer" + udid[0]

	mac := hmac.New(sha256.New, []byte(key))
	message := ""

	var keys []string
	for k, _ := range values {
		//log.Debug("K : ", k)
		keys = append(keys, k)
	}
	sort.Sort(utils.StringSlice(keys))

	for k := range keys {
		//log.Debug("K : ", keys[k], "V :", values.Get(keys[k]))
		message += keys[k] + values.Get(keys[k])
	}

	log.Debug("Hash message - ", message)
	mac.Write([]byte(message))
	expectedMAC := mac.Sum(nil)
	base64Hash := base64.StdEncoding.EncodeToString([]byte(expectedMAC))
	base64Hash = strings.Replace(base64Hash, "+", "", -1)

	//log.Debug("base64 of hash - ", base64Hash)
	return base64Hash
}

func GetMessageHashKey(mh string) string {
	key := fmt.Sprintf("%s:%s", MESSAGEHASH_KEY_PREFIX, mh)
	return key
}

func updateLastStarted(rc *redis.Client, curTime string) {
	err := rc.Set(GLOBAL_KEY_PREFIX+"_LastStarted", curTime)
	utils.QuitOnError(err)
}

// fetch key val map for Levels : 1-10:2;21-100:3;
// return levelMap array with default value in case of error.


func getFBTokenFromCache(uid string) string {
	key := USER_FB_TOKEN_PREFIX + ":" + uid
	fbToken, _ := rcVolatile.Get(key)
	return fbToken
}

func setFBTokenToCache(token, uid string) {
	key := USER_FB_TOKEN_PREFIX + ":" + uid
	rcVolatile.SetEx(key, 15*SECS_IN_DAY, token)
}

func validateFBToken(token string, snUid string, uid string) bool {
	log.Debug("FB token -", token, uid)
	// check from cache
	if getFBTokenFromCache(uid) == token {
		return true
	}
	// fetch from fb
	res, err := fb.Get("/me", fb.Params{"access_token": token})
	if err != nil {
		log.Error("error fetching fb token for uid- " + uid + " token- " + token)
		return false
	}
	if snUid == res["id"] {
		setFBTokenToCache(token, uid)
		return true
	}

	log.Info("FB token validation failed for uid - ", uid, res)
	return false
}

func validRequest(values url.Values, remoteCheck bool) bool {

	expectedMh, mhOK := values["mh"]
	_, udidOK := values["udid"]

	if !udidOK || !mhOK {
		log.Debug("no udid or mh")
		return false
	}

	delete(values, "mh")
	mh := GetHash(values)

	if mh == expectedMh[0] {

		if remoteCheck == true {
			yes, err := rcVolatile.Exists(GetMessageHashKey(mh))

			if err == nil && yes == true {
				return false
			} else {
				rcVolatile.SetEx(GetMessageHashKey(mh), 60*60*24*10, "true")
			}
		}

		return true
	}

	log.Info("MH comparision - %s - %s", mh, expectedMh[0])
	return false
}

