package scgame

import (

)

var PresenseServers = []string{"127.0.0.1:6379"}
var UserServers = []string{"127.0.0.1:6379"}
var UserGameServers = []string{"127.0.0.1:6379"}
var GameServers = []string{"127.0.0.1:6379"}
var UserListServers = []string{"127.0.0.1:6379"}
var VolatileServers = []string{"127.0.0.1:6379"}
var GameGoServers = []string{"127.0.0.1:8080"}
var LeaderboradServers = []string{"127.0.0.1:6379"}

var AnonMalePics = []string{
	"http://realteenpatti.com/images/anon/vk.png",
	"http://realteenpatti.com/images/anon/nm.png",
	"http://realteenpatti.com/images/anon/sk.png",
	"http://realteenpatti.com/images/anon/srk.png",
	"http://realteenpatti.com/images/anon/bo.png",
}

var AnonFemalePics = []string{
	"http://realteenpatti.com/images/anon/as.png",
}

var AnonMaleNames = []string{
	"Bademiyan",
	"CrimeMaster",
	"Stud",
	"Don",
	"Superstar",
	"Rockstar",
	"Prince",
	"Tiger",
	"Casanova",
	"Gambler",
	"Dancer",
	"Dawood",
}

var AnonFemaleNames = []string{
	"Diva",
	"Queen",
	"Cuddles",
	"Pretty",
	"Preity",
	"Lovely",
	"Cute",
	"Gorgeous",
	"Sweet",
	"Adorable",
	"Beauty Queen",
}

var AdminServer = "127.0.0.1"
var FluentServer = "127.0.0.1"
var StatsListServers = UserListServers

var ConfigPath = "./config/"

const (
	GLOBAL_KEY_PREFIX               = "sc"
	PL_KEY_PREFIX                   = "sc:pl"
	USER_KEY_PREFIX                 = "dg:user:uid"
	GAME_KEY_PREFIX                 = "dg:game:gid"
	USER_GAME_PREFIX                = "dg:usergame:uid"
	BOT_KEY_PREFIX                  = "sc:bot:uid"
	USERLIST_KEY_PREFIX             = "sc:userlist"
	USERLIST_UNINSTALLED_KEY_PREFIX = "sc:userlist:uninstalled"
	BOTLIST_KEY_PREFIX              = "sc:botlist"
	BOTLIST_FREE_KEY_PREFIX         = "sc:botlist:free"
	BOTLIST_PLAYING_KEY_PREFIX      = "sc:botlist:playing"
	GAMELIST_GAME_AVAILABLE         = "sc:gl:avl:lvl"
	GAMELIST_GAME_FULL              = "sc:gl:full:lvl"
	MESSAGEHASH_KEY_PREFIX          = "sc:mh"
	USER_FB_TOKEN_PREFIX            = "sc:user:uid:fbtoken"
	ONLINE_PN_QUEUE                 = "sc:pn:queue_online"
	ONLINE_PN_KEY                   = "sc:pn:uid"
	VOTD_RUNTIME_KEY                = "sc:runtime:votd"
	FB_INVITE_COOLDOWN_KEY          = "sc:cooldown:fb:invite"
	RATEUS_CD_TIMER_KEY             = "sc:rateus:cd:timer"
)

const MAX_CRICKETER_LEVEL = 100
const MIN_CRICKETER_LEVEL = 1
const MAX_PLAYERS = 2
const MIN_PLAYERS = 2
const DEFAULT_BOTS = 1
const GAME_ID = 1
const DEFAULT_STAT_TIME = 60000
const GAME_DEFAULT_WAIT_TIME = 3000
const GAME_DEFAULT_START_TIME = 2000
const GAME_SETTINGS_CHANGE_BUFFER_TIME = 8000
const GAME_DEFAULT_TURN_TIME = 25000
const GAME_DEFAULT_TURN_TIME_BUFFER = 100
const GAME_DEFAULT_CLOSING_TIME = 5000
const GAME_DEFAULT_BOOT_AMOUNT = 50
const GAME_DEFAULT_MAX_CHAAL = 800
const GAME_DEFAULT_TOTAL_POT = 7200
const GAME_DEFAULT_SIDESHOW_ACCEPT_DELAY = 10000
const GAME_DEFAULT_SIDESHOW_DELAY = 2000
const GOOGLE_STORE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmP+qMNroTQPl0QJSkvxGFR9t3DW9w3xQ+JJu0ih2vuFr0u8W7APX+7aHxMfOPTrwOhUpBivj6VUnno1T6+K5IqCdijLBoigILVPSd+hRO79uATwhpttMicIxjzc3v5poIjRUc4iu9PSA0ZHJNAp0Y19gfk4Q06iwWxINFOh0vxBDV/svwZIEEtq4zkOmcFLig4DdrRcEfoPNt7EyZLvlC+28L89esswmadBwaYIEyRwLtBjbAVBNAdnNmR28rlmVL76B6dAmF4Ae/Co3DgjZ+5krnZYIt7UYUVyAZHZJWGXGTsJCcbeA0qRSOg1Ps+pucTa/V7e2BVAbjGQKDLcENwIDAQAB"
const CONFIG_FOLDER_PATH = "./config/"
const APP_NAME = "Stick Cricket"
const BUNDLE_ID = "com.rmg.stickcricket"
const GOOGLE_ANALYTICS_PROPERTY_ID = "UA-50975487-1"
const GOOGLE_ANALYTICS_ENDPOINT = "http://www.google-analytics.com/collect"
const UPGRADE_GAME_LINK = "market://details?id=com.rmg.stickcricket"
const DEFAULT_START_CHIPS_AMOUNT = 100000
const SCORE_UPDATE_TURNTIME = 5

const SECS_IN_MINS = 60
const SECS_IN_HOURS = SECS_IN_MINS * 60
const SECS_IN_DAY = SECS_IN_HOURS * 24
const SECS_IN_WEEK = SECS_IN_DAY * 7

const DURATION_NEW_USER = SECS_IN_DAY
const DIFF_NEW_USER = SECS_IN_DAY
const DURATION_CURRENT_USER = SECS_IN_WEEK
const DIFF_CURRENT_USER = SECS_IN_WEEK
const DURATION_RETURNING_USER = SECS_IN_DAY
const LOCATION = "Asia/Kolkata"

const CUR_VERSION = 22

// This will change once we add runtime framework for level/content upload

