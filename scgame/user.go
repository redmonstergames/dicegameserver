package scgame

import (
	//"crypto/sha256"
	"errors"
	"fmt"
	"strconv"
//	"strings"
	"time"
	"net/http"
	"math/rand"


	"bitbucket.org/redmonstergames/stickcricketserver/json"
	"bitbucket.org/redmonstergames/stickcricketserver/log"
	"bitbucket.org/redmonstergames/stickcricketserver/utils"
)

const RATE_US_RUNTIME_FLAG = "rateus_fctr"
const RATE_US_RUNTIME_FLAG_NEW = "rateus_fctr_new"


func GetUserKey(uid string) string {
	key := fmt.Sprintf("%s:%s", USER_KEY_PREFIX, uid)
	return key
}

func (user *User) GetKey() string {
	key := fmt.Sprintf("%s:%s", USER_KEY_PREFIX, user.Uid)
	return key
}

func (user *User) GetTokenKey() string {
	key := fmt.Sprintf("%s:%s:%s", USER_KEY_PREFIX, "apptoken", user.Uid)
	return key
}

func (user *User) GetPurchaseKey() string {
	key := fmt.Sprintf("%s:%s:%s", USER_KEY_PREFIX, "iap", user.Uid)
	return key
}

func (user *User) Get9GameOrderKey() string {
	key := fmt.Sprintf("%s:%s:%s", USER_KEY_PREFIX, "9game", user.Uid)
	return key
}



func getStartChipAmount() float64 {
	return DEFAULT_START_CHIPS_AMOUNT
}

func (user *User) AddToUserlist() {
	rcUserList.ZAdd(USERLIST_KEY_PREFIX, strconv.FormatInt(time.Now().Unix(), 10), user.Uid)
	rcUserList.ZRem(USERLIST_UNINSTALLED_KEY_PREFIX, strconv.FormatInt(time.Now().Unix(), 10), user.Uid)
}

func (user *User) AddToUninstallList() {
	rcUserList.ZAdd(USERLIST_UNINSTALLED_KEY_PREFIX, strconv.FormatInt(time.Now().Unix(), 10), user.Uid)
	rcUserList.ZRem(USERLIST_KEY_PREFIX, strconv.FormatInt(time.Now().Unix(), 10), user.Uid)
}

var ErrorUserNotFound = errors.New("User not found")

func GetUser(uid string) (user *User, err error) {
	var userData string
	userData, err = rcUser.Get(GetUserKey(uid))
	user = new(User)

	if err == nil && userData != "" {
		err = json.Unmarshal([]byte(userData), user)
		utils.LogOnError(err)
		return user, err
	}
	log.Info("GetUser error - ", uid, err, userData)
	return nil, ErrorUserNotFound
}
func (user *User) Save() error {
	//now := uint64(time.Now().Unix())
	userData, err := json.Marshal(user)
	utils.QuitOnError(err)
	log.Info("key", user.GetKey())
	log.Info("value", string(userData))
	err = rcUser.Set(user.GetKey(), string(userData))
	if err != nil {
		log.Info("error recieved: ", err.Error())
	}
	return err
}

func (user *User) Create() error {
	userData, err := json.Marshal(user)
	log.Debug("user create err - ", user, err)
	utils.QuitOnError(err)

	err = rcUser.SetWithNx(user.GetKey(), string(userData))
	return err
}

func (user *User) Lock() string {
	tryCount := 3

	for tryCount > 0 {
		curTime := strconv.FormatInt(time.Now().Unix(), 10)
		err := rcUser.SetWithExNx(user.Uid+"_lock", curTime, 1)
		if err == nil { // got lock
			//log.Info("got lock....", user.Uid)
			return curTime
		}
		log.Info("trying aquiring lock....", user.Uid, err)
		time.Sleep(500 * time.Millisecond)
		tryCount--
	}

	log.Info("couldnt acquire lock....", user.Uid)
	return ""
}

func (user *User) Unlock(randLock string) {
	rcUser.Eval("if redis.call('get',KEYS[1]) == ARGV[1] then return redis.call('del',KEYS[1]) else return 0 end", 1,
		[]string{user.Uid + "_lock"}, []string{randLock})
}

func InitNewUser(uid string,snuid string, nid uint64, udid string) (user *User,err error){
	err = nil
	user = new(User)
	user.Uid = uid
	user.SnUid = snuid
	user.Nid = nid
	user.FirstName=""
	user.LastName=""
	user.Pic=""
	user.Age=""
	user.Gender=""
	user.Email=""
	user.Birthday=""
	user.Locale=""
	user.CreatedTime = uint64(time.Now().Unix())
	user.NumberofGamesWon = uint64(0)
	user.BestScore = uint64(0)
	user.Level = uint64(1)
	user.TypeOfDice = uint64(1)
	user.XPLevel = uint64(0)
	user.Traits = uint64(0)
	return user,err
}


func InitializeNewUser(w http.ResponseWriter,req *http.Request) (user *User,err error){
	if req.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	var requiredParams = []string{"snuid", "nid", "udid", }  //"devicetoken"}

	if(utils.HasRequiredParams(req,requiredParams) == false){
		return nil, errors.New("Invalid Params")
	}

	snuid := req.PostForm["snuid"]
	nid := req.PostForm["nid"]
	udid := req.PostForm["udid"]
	//deviceToken := req.PostForm["devicetoken"]
	var first_name,last_name ,email, gender, pic, locale string
	sn, _ := strconv.ParseUint(nid[0], 10, 64)

	if sn == NETWORK_FB { // need more information
		first_name = req.FormValue("first_name")

		if first_name == "" {
			log.Error("firstname  invalid params...")
			return nil, errors.New("Invalid Params")
		}

		last_name = req.FormValue("last_name")
		gender = req.FormValue("gender")
		email = req.FormValue("email")
		pic = req.FormValue("pic")
		locale = req.FormValue("locale")

	} else { // for anon use default
		first_name = AnonMaleNames[rand.Intn(len(AnonMaleNames))]
		pic = AnonMalePics[rand.Intn(len(AnonMalePics))]
		gender = "male"
	}
	genUid := BuildUid(nid[0],snuid[0])
	user,err = GetUser(genUid)
	var msg GameMessage
	if user != nil && err == nil {
		log.Info("User blob exist...bailing out...", genUid)
		msg = constructMessage(MESSAGE_USER_EXISTS,"")
		sendMessage(w, msg)
		return
	}

	user,err = InitNewUser(genUid,snuid[0],sn,udid[0])
	user.Email = email
	user.LastName = last_name
	user.Gender = gender
	user.Pic = pic
	user.Locale = locale
	err = user.Create()
	if(err == nil){
		return user, err
	}
	return nil,err

}

func (user *User) AddGametoUserList(gid string) (err error) {
	_,err = rcUserGamesList.SAdd(GetuserGameKey(user.Uid), gid)
	return err

}
func (user *User)RemoveGameFromUserList(gid string) (err  error){

	err = rcUserGamesList.SRem(GetuserGameKey(user.Uid), gid)
	return err

}