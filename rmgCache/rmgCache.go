// provides common caching infra
package rmgCache

import (
	"bitbucket.org/redmonstergames/stickcricketserver/json"
	"bitbucket.org/redmonstergames/stickcricketserver/redisPool"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"github.com/garyburd/redigo/redis"
)

type RMGCache struct {
	redisConnPool *redis.Pool
	cachePrefix   string
	cacheDuration uint64
}

// create a new RMGCache object
func New(server, cachePrefix string, duration uint64) *RMGCache {
	c := RMGCache{}
	c.redisConnPool = redisPool.GetPool(server)
	c.cachePrefix = cachePrefix
	c.cacheDuration = duration
	return &c
}

// add value to cache identified by key
func (c *RMGCache) AddToCache(key string, value interface{}) {
	c.AddToCacheWithExpiry(key, value, c.getVolatileCacheDuration())
}

// add value to cache identified by key with expiry in seconds
func (c *RMGCache) AddToCacheWithExpiry(key string, value interface{}, duration uint64) {
	if len(key) == 0 {
		return
	}
	if c.redisConnPool == nil {
		return
	}
	con := c.redisConnPool.Get()
	if con == nil {
		return
	}
	defer con.Close()
	data, err := json.Marshal(value)
	if err != nil {
		return
	}
	key = c.GenerateCacheKey(key)
	con.Do("SET", key, data, "EX", duration)
}

// fetch value form cache identified by key
func (c *RMGCache) FetchFromCache(key string, value interface{}) error {
	if len(key) == 0 {
		return errors.New("key cant be empty")
	}
	if c.redisConnPool == nil {
		return errors.New("redis volatile pool not initialized")
	}
	con := c.redisConnPool.Get()
	if con == nil {
		return errors.New("no connection")
	}
	defer con.Close()
	key = c.GenerateCacheKey(key)
	data, err := redis.Bytes(con.Do("GET", key))
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, value)
	if err != nil {
		return err
	}
	return nil
}

// generates a cache key for the given key
func (c *RMGCache) GenerateCacheKey(data string) string {
	hash := sha256.Sum256([]byte(data))
	value := base64.StdEncoding.EncodeToString(hash[:])
	return c.cachePrefix + value
}

func (c *RMGCache) getVolatileCacheDuration() uint64 {
	return c.cacheDuration
}

func (c *RMGCache) Delete(key string) error {
	if len(key) == 0 {
		return errors.New("key cant be empty")
	}
	if c.redisConnPool == nil {
		return errors.New("redis volatile pool not initialized")
	}
	con := c.redisConnPool.Get()
	if con == nil {
		return errors.New("no connection")
	}
	defer con.Close()
	key = c.GenerateCacheKey(key)
	con.Do("DEL", key)
	return nil
}
