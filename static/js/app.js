(function() {
    var app = angular.module('monsterAPI', ['ui.bootstrap', 'ngRoute']);

    //Routes
    app.config(function($routeProvider) {
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : 'pages/login.html',
            })

            // route for the dau page
            .when('/dau', {
                templateUrl : 'pages/dau.html',
            })

            // route for the dau page
            .when('/concurrents', {
                templateUrl : 'pages/concurrents.html',
            })

            // route for the admin page
            .when('/admin', {
                templateUrl : 'pages/admin.html',
            });
    });

    // Directives
    app.directive('navBar', function () {
        return {
            restrict: 'E',
            templateUrl: 'tmpl/navBar.html'
        };
    });

    app.directive('needsLogin', function(){
        return {
            restrict: 'E',
            controller: ['$http','$scope', function($http, $scope) {
                $scope.canShow = false;
                $http.get('/stats/login/')
                .error(function() {
                    window.location = "/";
                })
                .success(function() {
                $scope.canShow = true;
                })
            }]
    }});

    app.directive('loginPage', function() {
        return {
            restrict: 'E',
            templateUrl: 'tmpl/loginPage.html'
        };
    });

    app.directive('showDau', function() {
        return {
            restrict: 'E',
            templateUrl: 'tmpl/showDau.html'
        };
    });

    //Controller
    app.controller('URLController', ['$scope','$location', function($scope, $location) {
        $scope.navClass = function(page) {
            var currentRoute = $location.path().substring(1) || 'home';
            return page === currentRoute ? 'active' : '';
        };
    }]);

    app.controller('LoginController',['$http','$scope', function($http, $scope) {
        $scope.alerts = [];
        $scope.submit = function() {
            //alert(JSON.stringify($scope.user));
            $scope.alerts= [];
            $http({method: 'POST', url: '/stats/login/', data:JSON.stringify($scope.user)}).
                success(function(data, status, headers, config) {
                //alert(JSON.stringify(data));
                if (data.success == true) {
                    window.location.reload()
                }
            }).
            error(function(data, status, headers, config) {
                //alert(JSON.stringify(data));
                $scope.alerts = [{visible: true, msg:data.msg, type:'danger'}];
            });
        };
    }]);

})()
