FROM golang
MAINTAINER Ankur Shrivastava <ankur@redmonstergames.com>

RUN go get github.com/tools/godep
#
# copy source
ADD . /go/src/bitbucket.org/kiran_inbng/rmg_teenpattiserver

WORKDIR /go/src/bitbucket.org/kiran_inbng/rmg_teenpattiserver

RUN godep restore

RUN go install

RUN go install ./admin

## game server
EXPOSE 8080
## admin server
EXPOSE 8181
