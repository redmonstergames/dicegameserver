package engagement

import (
	"errors"
	"bitbucket.org/redmonstergames/stickcricketserver/utils"
	//"bitbucket.org/redmonstergames/stickcricketserver/log"
	"math"
)

type Engagement struct {
	Days  uint64		`json:"days"`
	State int		`json:"state"`
}

const (
	NONE = iota
	LOW_ENGAGED
	MEDIUM_ENGAGED
	HIGH_ENGAGED
	NORMAL
	INIT_ENGAGED
)

const ENGAGEMENT_RANGE = 7 // 7 days
const LOW_ENGAGEMENT = 3 // 3 days

// set the engagement : lastpld, now
func (engagement *Engagement) Set (lpld uint64) error {
	//find the day difference
	now := utils.GetISTTimeNow()	
	days := int (utils.DayDiff(now.Unix(), int64(lpld)))
	if days < 0 {
		return nil
	}
	days = days % (ENGAGEMENT_RANGE + 1)

	msb := int(highBitPos(engagement.Days))

	days += msb
	//log.Debug("[Engagement] engagement : ", days, *engagement, msb)
	switch {
		case days >= ENGAGEMENT_RANGE : {
			// reset the engagement data with carry fwd data from previous engagement.
			prevEngagement := engagement.Days >> uint(days - msb)
			preveEngagedNumber := highBitPos(prevEngagement)
			currDayBit := (preveEngagedNumber + uint(days - msb))
			if currDayBit >= ENGAGEMENT_RANGE {
				engagement.Days = 1
			} else {
				// carry fwd the low engagement from previous 7 days.
				engagement.Days = (1 << currDayBit) | prevEngagement
			}
		}
		case days >= 0 : {
			engagement.Days = ((1 << uint(days)) | engagement.Days)
		}
		default :
			return errors.New("Unable to Set Engagement")
	}
	
	count := countSetBits(engagement.Days)
	
	if count <= LOW_ENGAGEMENT {
		engagement.State = LOW_ENGAGED
	} else {
		engagement.State = NORMAL
	}	
	//log.Debug("[Engagement] final set engagement : ", *engagement)
	return nil;
}

func (engagement *Engagement) IsLowEngaged() bool {
	return (LOW_ENGAGED == engagement.State || INIT_ENGAGED == engagement.State)
}

func (engagement *Engagement) SetInitEngaged() {
	engagement.State = INIT_ENGAGED
}

func (engagement *Engagement) IsInitEngagement() bool {
	return (INIT_ENGAGED == engagement.State)
}

func highBitPos(val uint64) uint {
	return uint(math.Log2(float64(val)))
}

func countSetBits(val uint64) int {
	count :=0
	loop := val
	// Calculate no of 1's in the engagement.Days
	for i:=0; i < ENGAGEMENT_RANGE; i++ {
		if loop == 0 {
			break
		}
		loop = (loop) & (loop-1) // getting the no of 1's in the binary number.
		if loop == 0 {
			count++
			break;
		} else {
			count++
		}
	}
	
	return count
}
