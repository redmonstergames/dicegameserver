package main

import (
	"io"
	"net"
	"os"
	"strings"

	"bitbucket.org/redmonstergames/stickcricketserver/log"
)

const (
	CONN_PORT            = "843"
	CONN_TYPE            = "tcp"
	POLICTY_FILE_REQUEST = "policy-file-request"
	POLICY_FILE_NAME     = "./crossdomain.xml"
)

func main() {

	// Listen for incoming connections.
	l, err := net.Listen(CONN_TYPE, ":"+CONN_PORT)
	if err != nil {
		log.Error("Error listening:", err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()

	log.Debug("Listening on port :" + CONN_PORT)

	for {
		// accept incoming connection.
		conn, err := l.Accept()

		log.Debug("got connection request...")
		if err != nil {
			log.Error("Error accepting: ", err.Error())
			os.Exit(1)
		}

		// Handle connections in a new goroutine.
		go handleRequest(conn)
	}
}

// Handles incoming requests.
func handleRequest(conn net.Conn) {
	buf := make([]byte, 4096)
	defer conn.Close()

	log.Debug("Handling request from ", conn.RemoteAddr())
	n, err := conn.Read(buf)
	if err != nil || n == 0 {
		return
	}

	// allowing only <policy-file-request/>
	reqFile := string(buf[0:n])
	log.Debug("Recieved request for -", reqFile, "-", POLICTY_FILE_REQUEST)
	if strings.Contains(reqFile, POLICTY_FILE_REQUEST) {
		log.Debug("Sending policy file...")
		sendPolicyFile(conn)
	}

	log.Debug("Connection from %v closed.", conn.RemoteAddr())
}

func sendPolicyFile(conn net.Conn) {

	file, err := os.Open(POLICY_FILE_NAME)
	if err != nil {
		log.Error("policy file not found...", err)
	}
	defer file.Close()

	_, err = io.Copy(conn, file)
	if err != nil {
		log.Error("Not able to send polict file...", err)
	}
}
