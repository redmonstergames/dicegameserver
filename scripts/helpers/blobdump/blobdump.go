package main

import (
	"bitbucket.org/redmonstergames/stickcricketserver/redisPool"
	"bitbucket.org/redmonstergames/stickcricketserver/scgame"
	"bitbucket.org/redmonstergames/stickcricketserver/utils"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"os"
	"sync"
)

var UserPool *redis.Pool
var UserListPool *redis.Pool

var wg sync.WaitGroup
var userWG sync.WaitGroup
var notFound *os.File

func fetchBlob(c chan *string, uid string) {
	userWG.Add(1)
	wg.Add(1)
	defer userWG.Done()
	defer wg.Done()
	con := UserPool.Get()
	if con != nil {
		defer con.Close()
		user, err := redis.String(con.Do("GET", scgame.USER_KEY_PREFIX+":"+uid))
		if err == nil {
			c <- &user
		} else {
			fmt.Println("error user ", uid, err.Error())
			fmt.Fprintln(notFound, uid, " error ", err.Error())
		}
	}
}

func processUid(uidChan chan string, userChan chan *string) {
	wg.Add(1)
	defer wg.Done()
	for {
		count := 0
		if uid, ok := <-uidChan; ok {
			go fetchBlob(userChan, uid)
			count += 1
			if count == 1 {
				count = 0
				fmt.Println("waiting for user batch")
				// wait for user
				userWG.Wait()
				fmt.Println("batch finished")
			}
		} else {
			break
		}
	}
}

func doWait(c chan bool) {
	wg.Wait()
	c <- true
}

func main() {
	UserPool = redisPool.GetPool("twem:22122")
	UserListPool = redisPool.GetPool("stats:6379")
	uidChan := make(chan string, 10)
	userChan := make(chan *string, 10)
	done := make(chan bool)
	notFound, _ = os.Create("blobs-not-found.txt")
	defer notFound.Close()
	go utils.ScanForUids(uidChan, scgame.USERLIST_KEY_PREFIX, UserListPool)
	go processUid(uidChan, userChan)
	go doWait(done)
	f, err := os.Create("blobs.txt")
	defer f.Close()
	if err != nil {
		fmt.Println("error ", err.Error())
		return
	}
	fmt.Println("init done")
	for {
		select {
		case <-done:
			break
		case user := <-userChan:
			fmt.Fprintln(f, *user)
			//fmt.Println(*user)
		}
	}
}
