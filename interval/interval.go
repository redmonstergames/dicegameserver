package interval

import (
	"fmt"
    "github.com/biogo/store/interval"
	"bitbucket.org/redmonstergames/stickcricketserver/log"
)

// Integer-specific intervals
type IntInterval struct {
    Start, End int
    UID        uintptr
    Payload    interface{}
}

func (i IntInterval) Overlap(b interval.IntRange) bool {
    // Half-open interval indexing.
    return i.End >= b.Start && i.Start <= b.End
}

func (i IntInterval) ID() uintptr              { return i.UID }
func (i IntInterval) Range() interval.IntRange { return interval.IntRange{i.Start, i.End} }
func (i IntInterval) String() string           { return fmt.Sprintf("[%d,%d)#%d", i.Start, i.End, i.UID) }
func (i IntInterval) Data() interface{}        {return i.Payload}


//func (i IntInterval) SetStart(start int)    { i.Start = start }
//func (i IntInterval) SetEnd(end int)   { i.End = end }

//var ivs = []Interval{
//	{start: 0, end: 2},
//	{start: 2, end: 4},
//	{start: 5, end: 6},
//	{start: 3, end: 4},
//	{start: 1, end: 3},
//	{start: 4, end: 6},
//	{start: 5, end: 8},
//	{start: 6, end: 8},
//	{start: 5, end: 7},
//	{start: 8, end: 9},
//}

//func main() {
//	t := &interval.Tree{}
//	for i, iv := range ivs {
//		iv.id = uintptr(i)
//		intf := t.Get(iv)
//		if nil == intf {
//	 		err := t.Insert(iv, false)
//			if err != nil {
//				fmt.Println(err)
//			}
//		} else {
//			fmt.Println("error inserting interval, ", iv.String(), "not a exclusive interval")
//		}
//	}

//	fmt.Println("Generic interval tree:")
//	fmt.Println(t.Get(Interval{start: 3, end: 6}))
//}


func InsertPackageEx(t *interval.IntTree, intrvl IntInterval) bool {
	intf := t.Get(intrvl)
	if nil != intf {
		return false
	}
	
	intrvl.UID = uintptr(t.Len())
	
	err := t.Insert(intrvl, false)
	if err != nil {
		return false
	}
	
	return true
}

func InsertPackage(t *interval.IntTree, intrvl IntInterval) bool {
	
	intrvl.UID = uintptr(t.Len())
	
	err := t.Insert(intrvl, false)
	if err != nil {
		return false
	}
	
	return true
}

func FetchPayloadEx(t *interval.IntTree, elem int) interface{} {
	i := IntInterval {Start: elem, End: elem}
	
	intfs := t.Get(i)
	if nil == intfs || len(intfs) == 0 {
		log.Error("Not found interval for Start ", elem, " End:", elem)
		return nil
	}
	
	intf := intfs[0].(IntInterval)
	
	return intf.Payload
}

func FetchPayload(t *interval.IntTree, elem int) []interface{} {
	i := IntInterval {Start: elem, End: elem}
	
	intfs := t.Get(i)
	if nil == intfs || len(intfs) == 0 {
		log.Error("Not found interval for Start ", elem, " End:", elem)
		return nil
	}
	var payload []interface{}
	
	for i := range intfs {
		intf := intfs[i].(IntInterval)
		payload = append(payload, intf.Payload)		
	}
	
	return payload
}
