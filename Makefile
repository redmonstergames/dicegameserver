ifeq ($(INSANE),true)
	ARGS=-a -v
endif
ifeq ($(RACE),true)
	ARGS=-race
endif

PORT = 8080

all: build_all

build_all: clean build stickcricketserver adminserver test

build:
	godep go build $(ARGS) ./...

test:
	godep go test ./...

adminserver:
	godep go build $(ARGS) admin/adminserver.go

stickcricketserver:
	godep go build $(ARGS) .

mrt_cmd:
	godep go build $(ARGS) -o mrt_cmd mrt/cmd/mrt.go

socketserver:
	godep go build $(ARGS) -o socketserver policyserver/policyserver.go

clean:
	go clean ./...
	rm -f stickcricketserver adminserver tpsrv mrt_cmd socketserver

run: clean build stickcricketserver
	        nohup ./stickcricketserver -s $(PORT) >> ./app.log  2> ./appErr.log &

runadmin: clean build adminserver
	        nohup ./adminserver -s 9090 2>&1 >> ./admin.log &

reload:
	kill -1 `ps aux | grep stickcricketserver |  grep -v grep | awk '{print $$2}'`

index:
	godoc -index -http=:6060

list:
	go list ./...

#runall:
#	./stickcricketserver &
#	./adminserver -s 9090 &

#kill:
#	kill -9 `ps aux | grep sticksricketserver |  grep -v grep | awk '{print $$2}'`
#	kill -9 `ps aux | grep adminserver |  grep -v grep | awk '{print $$2}'`


